<?php

class ControllerShippingBringShipping extends Controller
{

    private $error = array();

    public function index()
    {

        $this->load->language('shipping/bringshipping');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('bringshipping', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['entry_rate'] = $this->language->get('entry_rate');
        $this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['entry_groups'] = $this->language->get('entry_groups');
        $this->data['entry_store'] = $this->language->get('entry_store');
        $this->data['entry_general'] = $this->language->get('entry_general');
        $this->data['entry_limit_cart'] = $this->language->get('entry_limit_cart');
        $this->data['entry_min_limit'] = $this->language->get('entry_min_limit');
        $this->data['entry_max_limit'] = $this->language->get('entry_max_limit');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_exclude'] = $this->language->get('entry_exclude');
        $this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['text_default'] = $this->language->get('text_default');
        $this->data['text_all_customers'] = $this->language->get('text_all_customers');
        $this->data['date_start'] = $this->language->get('date_start');
        $this->data['date_end'] = $this->language->get('date_end');
        $this->data['date_period'] = $this->language->get('date_period');
        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_weight'] = $this->language->get('tab_weight');
        $this->data['tab_flat'] = $this->language->get('tab_flat');
        $this->data['entry_flat'] = $this->language->get('entry_flat');
        $this->data['text_flat_rate'] = $this->language->get('text_flat_rate');
        $this->data['text_per_item'] = $this->language->get('text_per_item');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['use_dates'] = $this->language->get('use_dates');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_shipping'),
            'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('shipping/bringshipping', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('shipping/bringshipping', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['weight_modules'] = array();
        $this->data['flat_modules'] = array();

        if (isset($this->request->post['bringshipping_weight'])) {
            $this->data['weight_modules'] = $this->request->post['bringshipping_weight'];
        } elseif ($this->config->get('bringshipping_weight')) {
            $this->data['weight_modules'] = $this->config->get('bringshipping_weight');
        }


        if (isset($this->request->post['bringshipping_flat'])) {
            $this->data['flat_modules'] = $this->request->post['bringshipping_flat'];
        } elseif ($this->config->get('bringshipping_flat')) {
            $this->data['flat_modules'] = $this->config->get('bringshipping_flat');
        }

        $this->data['module_status'] = $this->config->get('bringshipping_status');
        $this->data['weight_status'] = $this->config->get('bringshipping_weight_status');
        $this->data['flat_status'] = $this->config->get('bringshipping_flat_status');


        /*get the customer groups list*/

        $this->load->model('sale/customer_group');
        $this->data['groups'] = $this->model_sale_customer_group->getCustomerGroups();

        /*get the store list*/
        $this->data['stores'][] = array(
            'store_id' => 0,
            'name' => $this->config->get('config_name') . $this->language->get('text_default')
        );

        $this->load->model('setting/store');
        $results = $this->model_setting_store->getStores();

        foreach ($results as $result) {

            $this->data['stores'][] = array(
                'store_id' => $result['store_id'],
                'name' => $result['name']
            );
        }

        /*get the geozones  list*/
        $this->load->model('localisation/geo_zone');
        $geo_zones = $this->model_localisation_geo_zone->getGeoZones();
        $this->data['geo_zones'] = $geo_zones;

        $this->load->model('localisation/tax_class');
        $this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
        $this->template = 'shipping/bringshipping.php';

        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'shipping/bringshipping')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Action on module installation
     */
    public function install()
    {
        try {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD COLUMN `bring_shipping` TEXT DEFAULT NULL");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Action an module uninstall
     */
    public function uninstall()
    {
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` DROP COLUMN `bring_shipping`");
    }
}