<?php

/**
 * Class ControllerShippingBring
 */
class ControllerShippingBring extends Controller
{

    /**
     * Error holder
     * @var array
     */
    public $error = array();

    /**
     * Index - standard method on accessing the module
     */
    public function index()
    {

        $this->language->load('shipping/bring');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');
        $this->load->model('localisation/tax_class');
        $this->load->model('localisation/geo_zone');

        /*
         * Form submission check
         */
        if (($this->request->server['REQUEST_METHOD'] === 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('bring', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
        }

        /*
         * Translation
         */
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['entry_price'] = $this->language->get('entry_price');
        $this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $this->data['entry_status'] = $this->language->get('entry_status');

        $this->data['entry_site_url'] = $this->language->get('entry_site_url');
        $this->data['entry_site_url_helper'] = $this->language->get('entry_site_url_helper');
        $this->data['entry_from_postal_code'] = $this->language->get('entry_from_postal_code');
        $this->data['entry_from_postal_code_helper'] = $this->language->get('entry_from_postal_code_helper');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $this->data['entry_min_price'] = $this->language->get('entry_min_price');
        $this->data['entry_min_price_helper'] = $this->language->get('entry_min_price_helper');
        $this->data['entry_max_price'] = $this->language->get('entry_max_price');
        $this->data['entry_max_price_helper'] = $this->language->get('entry_max_price_helper');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['tab_general'] = $this->language->get('tab_general');

        /*
         * Treat any errors
         */
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        /*
         * Breadcrumbs breadcrumbs breadcrumbs
         */
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_shipping'),
            'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('shipping/bring', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        /*
         * Action buttons
         */
        $this->data['action'] = $this->url->link('shipping/bring', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

        /* ------------------------------------------------------------------------------------------------------------
         * GEO ZONES & Tax classes
         * ------------------------------------------------------------------------------------------------------------
         */
        $geo_zones = $this->model_localisation_geo_zone->getGeoZones();
        $this->data['geo_zones'] = $geo_zones;
        $this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        foreach ($geo_zones as $geo_zone) {
            // Tax classes
            if (isset($this->request->post["bring_tax_class_id_{$geo_zone['geo_zone_id']}"])) {
                $this->data["bring_tax_class_id_{$geo_zone['geo_zone_id']}"] = $this->request->post["bring_tax_class_id_{$geo_zone['geo_zone_id']}"];
            } else {
                $this->data["bring_tax_class_id_{$geo_zone['geo_zone_id']}"] = $this->config->get("bring_tax_class_id_{$geo_zone['geo_zone_id']}");
            }
            // Price
            if (isset($this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_price'])) {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_price'] = $this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_price'];
            } else {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_price'] = $this->config->get('bring_' . $geo_zone['geo_zone_id'] . '_price');
            }
            // Min price
            if (isset($this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_min_price'])) {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_min_price'] = $this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_min_price'];
            } else {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_min_price'] = $this->config->get('bring_' . $geo_zone['geo_zone_id'] . '_min_price');
            }
            // Max price
            if (isset($this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_max_price'])) {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_max_price'] = $this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_max_price'];
            } else {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_max_price'] = $this->config->get('bring_' . $geo_zone['geo_zone_id'] . '_max_price');
            }
            // Status
            if (isset($this->request->post['weight_' . $geo_zone['geo_zone_id'] . '_status'])) {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_status'] = $this->request->post['bring_' . $geo_zone['geo_zone_id'] . '_status'];
            } else {
                $this->data['bring_' . $geo_zone['geo_zone_id'] . '_status'] = $this->config->get('bring_' . $geo_zone['geo_zone_id'] . '_status');
            }
        }

        /*
         * Status
         */
        if (isset($this->request->post['bring_status'])) {
            $this->data['bring_status'] = $this->request->post['bring_status'];
        } else {
            $this->data['bring_status'] = $this->config->get('bring_status');
        }

        /*
         * Site URL
         */
        if (isset($this->request->post['bring_site_url'])) {
            $this->data['bring_site_url'] = $this->request->post['bring_site_url'];
        } else {
            $this->data['bring_site_url'] = HTTP_CATALOG;
        }

        /*
         * Shop postal code
         */
        if (isset($this->request->post['bring_from_postal_code'])) {
            $this->data['bring_from_postal_code'] = $this->request->post['bring_from_postal_code'];
        } else {
            $this->data['bring_from_postal_code'] = $this->config->get('bring_from_postal_code');
        }

        /*
         * Sort order
         */
        if (isset($this->request->post['bring_sort_order'])) {
            $this->data['bring_sort_order'] = $this->request->post['bring_sort_order'];
        } else {
            $this->data['bring_sort_order'] = $this->config->get('bring_sort_order');
        }

        /*
         * Load the templates
         */
        $this->template = 'shipping/bring.php';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        /*
         * Output
         */
        $this->response->setOutput($this->render());
    }


    /**
     * Validate $_POST request
     * @return bool
     */
    public function validate()
    {
        if (!$this->user->hasPermission('modify', 'shipping/bring')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Action on module installation
     */
    public function install()
    {
        try {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD COLUMN `bring_shipping` TEXT DEFAULT NULL");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Action an module uninstall
     */
    public function uninstall()
    {
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` DROP COLUMN `bring_shipping`");
    }
}
