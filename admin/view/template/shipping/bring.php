<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">

            <div class="vtabs">
                <!-- General settings -->
                <a href="#tab-general">
                    <?php echo $tab_general; ?>
                </a>
                <!-- Tabs for geo zones -->
                <?php foreach ($geo_zones as $geo_zone) : ?>
                <a href="#tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></a>
                <?php endforeach; ?>
            </div>

            <!-- The form -->
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="tab-general" class="vtabs-content">
                    <table class="form">

                        <!-- General status -->
                        <tr>
                            <td><?php echo $entry_status; ?></td>
                            <td><select name="bring_status">
                                    <?php if ($bring_status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select></td>
                        </tr>

                        <!-- Site URL -->
                        <input type="hidden" name="bring_site_url" value="<?php echo $bring_site_url; ?>" size="30" />

                        <!-- Postal code -->
                        <tr>
                            <td>
                                <?php echo $entry_from_postal_code; ?>
                                <div class="help"><?php echo $entry_from_postal_code_helper; ?></div>
                            </td>
                            <td><input type="number" name="bring_from_postal_code" value="<?php echo $bring_from_postal_code; ?>" size="30" /></td>
                        </tr>

                        <!-- Sort order -->
                        <tr>
                            <td>
                                <?php echo $entry_sort_order; ?>
                            </td>
                            <td><input type="number" name="bring_sort_order" value="<?php echo $bring_sort_order; ?>" size="1" /></td>
                        </tr>
                    </table>
                </div>

                <?php //echo '<pre>'; print_r($geo_zones); echo '</pre>'; ?>

                <!--
                ----------------------------------------------------------------------------------------------
                GEO ZONES
                ----------------------------------------------------------------------------------------------
                -->
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <div id="tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>" class="vtabs-content">
                    <table class="form">

                        <!-- Tax class -->
                        <tr>
                            <td>
                                <?php echo $entry_tax_class; ?>
                            </td>
                            <td>
                                <select name="bring_tax_class_id_<?php echo $geo_zone['geo_zone_id']; ?>">
                                    <option value="0"><?php echo $text_none; ?></option>
                                    <?php foreach ($tax_classes as $tax_class) { ?>
                                        <?php if (${'bring_tax_class_id_' . $geo_zone['geo_zone_id']} == $tax_class['tax_class_id']) { ?>
                                            <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <!-- Price -->
                        <tr>
                            <td>
                                <?php echo $entry_price; ?>
                            </td>
                            <td>
                                <?php
                                    $price = ${'bring_' . $geo_zone['geo_zone_id'] . '_price'};
                                    $price = (!empty($price)) ? $price : 0;
                                    echo "<input type='number' name='bring_{$geo_zone['geo_zone_id']}_price' value='{$price}' />"; 
                                ?>
                            </td>
                        </tr>

                        <!-- Min price -->
                        <tr>
                            <td>
                                <?php echo $entry_min_price; ?>
                                <div class="help"><?php echo $entry_min_price_helper; ?></div>
                            </td>
                            <td>
                                <?php
                                    $min_price = ${'bring_' . $geo_zone['geo_zone_id'] . '_min_price'};
                                    $min_price = (!empty($min_price)) ? $min_price : 0;
                                    echo "<input type='number' name='bring_{$geo_zone['geo_zone_id']}_min_price' value='{$min_price}' />";
                                ?>
                            </td>
                        </tr>

                        <!-- Max price -->
                        <tr>
                            <td>
                                <?php echo $entry_max_price; ?>
                                <div class="help"><?php echo $entry_max_price_helper; ?></div>
                            </td>
                            <td>
                                <?php
                                $max_price = ${'bring_' . $geo_zone['geo_zone_id'] . '_max_price'};
                                $max_price = (!empty($max_price)) ? $max_price : 0;
                                echo "<input type='number' name='bring_{$geo_zone['geo_zone_id']}_max_price' value='{$max_price}' />";
                                ?>
                            </td>
                        </tr>

                        <!-- Status -->
                        <tr>
                            <td><?php echo $entry_status; ?></td>
                            <td><select name="bring_<?php echo $geo_zone['geo_zone_id']; ?>_status">
                                    <?php if (${'bring_' . $geo_zone['geo_zone_id'] . '_status'}) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select></td>
                        </tr>
                    </table>
                </div>
                <?php } ?><!-- End Geo zones -->
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('.vtabs a').tabs();
    //--></script>
<?php echo $footer; ?>