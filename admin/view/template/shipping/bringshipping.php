<?php echo $header; ?>

<div id="content">

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a
            href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>


    <div class="box">
        <div class="heading">
            <h1><img src="view/image/shipping.png" alt=""/> <?php echo $heading_title; ?></h1>


            <div class="buttons"><a onclick="submitForm();" class="button"><?php echo $button_save; ?></a><a
                    onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>

        <div class="content">

            <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a><a
                    href="#tab-weight"><?php echo $tab_weight; ?></a><a href="#tab-flat"><?php echo $tab_flat; ?></a>
            </div>


            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

                <div id="tab-general">
                    Overall Status:

                    <select name="bringshipping_status">
                        <?php if ($module_status['bringshipping_status']) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select><br/>
                </div>
                <div id="tab-weight">

                    <table id="module" class="list">
                        <thead>
                        <tr>
                            <td class="left"><?php echo $entry_general; ?></td>
                            <td class="left"><?php echo $entry_store; ?></td>
                            <td class="left"><?php echo $entry_groups; ?></td>
                            <td class="left"><?php echo $entry_geo_zone; ?></td>
                            <td class="left"><?php echo $entry_exclude; ?></td>
                            <td class="left"><?php echo $entry_limit_cart; ?></td>

                            <td class="left"><?php echo $entry_rate; ?></td>
                            <td class="left"><?php echo $date_period; ?></td>
                            <td></td>
                        </tr>
                        </thead>


                        <?php $module_row = 0; ?>
                        <?php foreach ($weight_modules as $module) { ?>

                            <tbody id="module-row<?php echo $module_row; ?>">

                            <tr>
                                <td>
                                    <?php echo $entry_status; ?>

                                    <select name="bringshipping_weight[<?php echo $module_row; ?>][status]">
                                        <?php if ($module['status']) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td rowspan="4">

                                    <?php if ($stores) { ?>
                                        <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">
                                            <?php foreach ($stores as $store) { ?>

                                                <li><input type="checkbox"
                                                           name="bringshipping_weight[<?php echo $module_row; ?>][stores][]"
                                                           value="<?php echo $store['store_id'] ?>"<?php if (isset($module['stores']) && in_array($store['store_id'], $module['stores'])) {
                                                        echo " checked=\"checked\"";
                                                    } ?> /> <?php echo $store['name']; ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </td>
                                <td rowspan="4">

                                    <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">

                                        <li><input type="checkbox"
                                                   name="bringshipping_weight[<?php echo $module_row; ?>][all_customers]"
                                                   value="1" <?php if (isset($module['all_customers']) && $module['all_customers']) {
                                                echo " checked=\"checked\"";
                                            } ?> /> <?php echo $text_all_customers; ?> </li>
                                        <?php if ($groups) { ?>

                                            <?php foreach ($groups as $group) { ?>

                                                <li><input type="checkbox"
                                                           name="bringshipping_weight[<?php echo $module_row; ?>][customer_groups][]"
                                                           value="<?php echo $group['customer_group_id'] ?>" <?php if (isset($module['customer_groups']) && in_array($group['customer_group_id'], $module['customer_groups'])) {
                                                        echo " checked=\"checked\"";
                                                    } ?> /> <?php echo $group['name']; ?></li>
                                            <?php } ?>

                                        <?php } ?>
                                    </ul>
                                </td>
                                <td rowspan="4" style=" ">
                                    <ul style="height: 100%; width: 250px;list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">

                                        <li><input type="checkbox"
                                                   name="bringshipping_weight[<?php echo $module_row; ?>][all_zones]"
                                                   value="1" <?php if (isset($module['all_zones']) && $module['all_zones']) {
                                                echo " checked=\"checked\"";
                                            } ?> /> <?php echo $text_all_zones; ?> </li>

                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                            <li>
                                                <input type="checkbox"
                                                       name="bringshipping_weight[<?php echo $module_row; ?>][geo_zones][]"
                                                       value="<?php echo $geo_zone['geo_zone_id'] ?>" <?php if (isset($module['geo_zones']) && in_array($geo_zone['geo_zone_id'], $module['geo_zones'])) {
                                                    echo "checked=\"checked\"";
                                                } ?> /> <?php echo $geo_zone['name']; ?>
                                            </li>

                                        <?php } ?>
                                    </ul>


                                </td>
                                <td rowspan="4" style=" ">
                                    <ul style="height: 100%; width: 250px;list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">


                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                            <li>
                                                <input type="checkbox"
                                                       name="bringshipping_weight[<?php echo $module_row; ?>][geo_zones_excluded][]"
                                                       value="<?php echo $geo_zone['geo_zone_id'] ?>" <?php if (isset($module['geo_zones_excluded']) && in_array($geo_zone['geo_zone_id'], $module['geo_zones_excluded'])) {
                                                    echo "checked=\"checked\"";
                                                } ?> /> <?php echo $geo_zone['name']; ?>
                                            </li>

                                        <?php } ?>
                                    </ul>

                                </td>

                                <td rowspan="4"><?php echo $entry_min_limit; ?>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][cart_min_limit]" <?php if (isset($module['cart_min_limit'])) {
                                        echo "value=\"" . $module['cart_min_limit'] . "\"";
                                    } ?> size="10"/> <br/> <br/>
                                    <?php echo $entry_max_limit; ?>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][cart_max_limit]" <?php if (isset($module['cart_max_limit'])) {
                                        echo "value=\"" . $module['cart_max_limit'] . "\"";
                                    } ?> size="10"/><br/> <br/>

                                </td>


                                <td rowspan="4">
                                    <textarea name="bringshipping_weight[<?php echo $module_row; ?>][rates]"
                                              cols="40" rows="5"><?php if (isset($module['rates'])) {
                                            echo $module['rates'];
                                        } ?></textarea>
                                </td>
                                <td rowspan="4">

                                    <?php echo $use_dates; ?>

                                    <input type="checkbox"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][use_dates]"
                                           class="use_dates" <?php if (isset($module['use_dates'])) {
                                        echo "checked=\"checked\"";
                                    } ?> />
                                    <br/>

                                    <?php echo $date_start; ?><br/>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][date_start]"
                                           class="datetime" <?php if (isset($module['date_start'])) {
                                        echo "value=\"" . $module['date_start'] . "\"";
                                    } ?> /> <br/>
                                    <?php echo $date_end; ?><br/>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][date_end]"
                                           class="datetime" <?php if (isset($module['date_end'])) {
                                        echo "value=\"" . $module['date_end'] . "\"";
                                    } ?> />

                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $entry_title; ?>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][title]" <?php if (isset($module['title'])) {
                                        echo "value=\"" . $module['title'] . "\"";
                                    } ?> size="25"/>


                                </td>
                                <td class="right">
                                    <a onclick="$('#module-row<?php echo $module_row; ?>').remove();"
                                       class="button"><span>Remove</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <?php echo $entry_tax_class; ?>


                                    <select name="bringshipping_weight[<?php echo $module_row; ?>][tax_class]">
                                        <option value="0"><?php echo $text_none; ?></option>
                                        <?php foreach ($tax_classes as $tax_class) { ?>
                                            <?php if ($tax_class['tax_class_id'] == $module['tax_class']) { ?>
                                                <option value="<?php echo $tax_class['tax_class_id']; ?>"
                                                        selected="selected"><?php echo $tax_class['title']; ?></option>
                                            <?php } else { ?>
                                                <option
                                                    value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td rowspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $entry_sort_order; ?>
                                    <input type="text"
                                           name="bringshipping_weight[<?php echo $module_row; ?>][sort_order]" <?php if (isset($module['sort_order'])) {
                                        echo "value=\"" . $module['sort_order'] . "\"";
                                    } ?> size="3"/>

                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td colspan="9" style="background-color: #EFEFEF;"></td>
                            </tr>
                            </tbody>

                            <?php $module_row++; ?>
                        <?php } ?>


                        <tfoot>
                        <tr>
                            <td colspan="8"></td>
                            <td class="right"><a onclick="addWeight();" class="button"><span>Add</span></a></td>
                        </tr>
                        </tfoot>
                    </table>

                    Overall Weight rates Status:

                    <select name="bringshipping_weight_status">
                        <?php if ($weight_status['bringshipping_weight_status']) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select><br/>

                </div>


                <div id="tab-flat">

                    <table id="flat" class="list">
                        <thead>
                        <tr>
                            <td class="left"><?php echo $entry_general; ?></td>
                            <td class="left"><?php echo $entry_store; ?></td>
                            <td class="left"><?php echo $entry_groups; ?></td>
                            <td class="left"><?php echo $entry_geo_zone; ?></td>
                            <td class="left"><?php echo $entry_exclude; ?></td>
                            <td class="left"><?php echo $entry_limit_cart; ?></td>

                            <td class="left"><?php echo $entry_flat; ?></td>
                            <td class="left"><?php echo $date_period; ?></td>
                            <td></td>
                        </tr>
                        </thead>


                        <?php $flat_row = 0; ?>
                        <?php foreach ($flat_modules as $module) { ?>

                            <tbody id="flat-row<?php echo $flat_row; ?>">

                            <tr>
                                <td>
                                    <?php echo $entry_status; ?>

                                    <select name="bringshipping_flat[<?php echo $flat_row; ?>][status]">
                                        <?php if ($module['status']) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td rowspan="4">

                                    <?php if ($stores) { ?>
                                        <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">
                                            <?php foreach ($stores as $store) { ?>

                                                <li><input type="checkbox"
                                                           name="bringshipping_flat[<?php echo $flat_row; ?>][stores][]"
                                                           value="<?php echo $store['store_id'] ?>"<?php if (isset($module['stores']) && in_array($store['store_id'], $module['stores'])) {
                                                        echo " checked=\"checked\"";
                                                    } ?> /> <?php echo $store['name']; ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </td>
                                <td rowspan="4">

                                    <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">

                                        <li><input type="checkbox"
                                                   name="bringshipping_flat[<?php echo $flat_row; ?>][all_customers]"
                                                   value="1" <?php if (isset($module['all_customers']) && $module['all_customers']) {
                                                echo " checked=\"checked\"";
                                            } ?> /> <?php echo $text_all_customers; ?> </li>
                                        <?php if ($groups) { ?>

                                            <?php foreach ($groups as $group) { ?>

                                                <li><input type="checkbox"
                                                           name="bringshipping_flat[<?php echo $flat_row; ?>][customer_groups][]"
                                                           value="<?php echo $group['customer_group_id'] ?>" <?php if (isset($module['customer_groups']) && in_array($group['customer_group_id'], $module['customer_groups'])) {
                                                        echo " checked=\"checked\"";
                                                    } ?> /> <?php echo $group['name']; ?></li>
                                            <?php } ?>

                                        <?php } ?>
                                    </ul>
                                </td>
                                <td rowspan="4" style=" ">
                                    <ul style="height: 100%; width: 250px; list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">
                                        <li><input type="checkbox"
                                                   name="bringshipping_flat[<?php echo $flat_row; ?>][all_zones]"
                                                   value="1" <?php if (isset($module['all_zones']) && $module['all_zones']) {
                                                echo " checked=\"checked\"";
                                            } ?> /> <?php echo $text_all_zones; ?> </li>


                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                            <li>
                                                <input type="checkbox"
                                                       name="bringshipping_flat[<?php echo $flat_row; ?>][geo_zones][]"
                                                       value="<?php echo $geo_zone['geo_zone_id'] ?>" <?php if (isset($module['geo_zones']) && in_array($geo_zone['geo_zone_id'], $module['geo_zones'])) {
                                                    echo "checked=\"checked\"";
                                                } ?> /> <?php echo $geo_zone['name']; ?>
                                            </li>

                                        <?php } ?>
                                    </ul>


                                </td>
                                <td rowspan="4" style=" ">
                                    <ul style="height: 100%; width: 250px;list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;">


                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                            <li>
                                                <input type="checkbox"
                                                       name="bringshipping_flat[<?php echo $flat_row; ?>][geo_zones_excluded][]"
                                                       value="<?php echo $geo_zone['geo_zone_id'] ?>" <?php if (isset($module['geo_zones_excluded']) && in_array($geo_zone['geo_zone_id'], $module['geo_zones_excluded'])) {
                                                    echo "checked=\"checked\"";
                                                } ?> /> <?php echo $geo_zone['name']; ?>
                                            </li>

                                        <?php } ?>
                                    </ul>


                                <td rowspan="4"><?php echo $entry_min_limit; ?>
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][cart_min_limit]" <?php if (isset($module['cart_min_limit'])) {
                                        echo "value=\"" . $module['cart_min_limit'] . "\"";
                                    } ?> size="10"/> <br/> <br/>
                                    <?php echo $entry_max_limit; ?>
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][cart_max_limit]" <?php if (isset($module['cart_max_limit'])) {
                                        echo "value=\"" . $module['cart_max_limit'] . "\"";
                                    } ?> size="10"/><br/> <br/>

                                </td>


                                </td>
                                <td rowspan="4">
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][flat_rate]" <?php if (isset($module['flat_rate'])) {
                                        echo "value=\"" . $module['flat_rate'] . "\"";
                                    } ?> />

                                    <br/> <br/>
                                    <select name="bringshipping_flat[<?php echo $flat_row; ?>][type]">
                                        <?php if (isset($module['type']) && $module['type'] == 'per_item') { ?>
                                            <option value="per_item"
                                                    selected="selected"><?php echo $text_per_item; ?></option>
                                            <option value="flat_rate"><?php echo $text_flat_rate; ?></option>
                                        <?php } else { ?>
                                            <option value="per_item"><?php echo $text_per_item; ?></option>
                                            <option value="flat_rate"
                                                    selected="selected"><?php echo $text_flat_rate; ?></option>
                                        <?php } ?>
                                    </select>


                                </td>
                                <td rowspan="4">
                                    <?php echo $use_dates; ?>

                                    <input type="checkbox"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][use_dates]"
                                           class="use_dates" <?php if (isset($module['use_dates'])) {
                                        echo "checked=\"checked\"";
                                    } ?> />
                                    <br/>


                                    <?php echo $date_start; ?><br/>
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][date_start]"
                                           class="datetime" <?php if (isset($module['date_start'])) {
                                        echo "value=\"" . $module['date_start'] . "\"";
                                    } ?> /> <br/>
                                    <?php echo $date_end; ?><br/>
                                    <input type="text" name="bringshipping_flat[<?php echo $flat_row; ?>][date_end]"
                                           class="datetime" <?php if (isset($module['date_end'])) {
                                        echo "value=\"" . $module['date_end'] . "\"";
                                    } ?> />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $entry_title; ?>
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][title]" <?php if (isset($module['title'])) {
                                        echo "value=\"" . $module['title'] . "\"";
                                    } ?> size="25"/>


                                </td>
                                <td class="right">
                                    <a onclick="$('#flat-row<?php echo $flat_row; ?>').remove();" class="button"><span>Remove</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <?php echo $entry_tax_class; ?>


                                    <select name="bringshipping_flat[<?php echo $flat_row; ?>][tax_class]">
                                        <option value="0"><?php echo $text_none; ?></option>
                                        <?php foreach ($tax_classes as $tax_class) { ?>
                                            <?php if ($tax_class['tax_class_id'] == $module['tax_class']) { ?>
                                                <option value="<?php echo $tax_class['tax_class_id']; ?>"
                                                        selected="selected"><?php echo $tax_class['title']; ?></option>
                                            <?php } else { ?>
                                                <option
                                                    value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td rowspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $entry_sort_order; ?>
                                    <input type="text"
                                           name="bringshipping_flat[<?php echo $flat_row; ?>][sort_order]" <?php if (isset($module['sort_order'])) {
                                        echo "value=\"" . $module['sort_order'] . "\"";
                                    } ?> size="3"/>

                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td colspan="9" style="background-color: #EFEFEF;"></td>
                            </tr>
                            </tbody>

                            <?php $flat_row++; ?>
                        <?php } ?>


                        <tfoot>
                        <tr>
                            <td colspan="8"></td>
                            <td class="right"><a onclick="addFlat();" class="button"><span>Add</span></a></td>
                        </tr>
                        </tfoot>
                    </table>


                    Overall Flat rates Status:

                    <select name="bringshipping_flat_status">
                        <?php if ($flat_status['bringshipping_flat_status']) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select><br/>


                </div>


            </form>
        </div>
    </div>

</div>
<script type="text/javascript">
    var module_row =<?php echo $module_row; ?>;

    function addWeight() {

        html = '<tbody id="module-row' + module_row + '">';
        html += '  <tr>  ';
        html += '  <td> <?php echo $entry_status; ?>';
        html += '      <select name="bringshipping_weight[' + module_row + '][status]">';
        html += '<option value="0"><?php echo $text_disabled; ?></option>   ';
        html += '<option value="1"><?php echo $text_enabled; ?></option>   ';
        html += ' </select> </td> ';
        html += '  <td rowspan="4" >  ';


        <?php if ($stores) { ?>
        html += ' <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" >';
        <?php foreach ($stores as $store) { ?>

        html += '   <li ><input type="checkbox" name="bringshipping_weight[' + module_row + '][stores][]"  value="<?php echo $store['store_id']?>"  /> <?php echo $store['name']; ?></li>';
        <?php } ?>
        html += '  </ul>';
        <?php }  ?>


        html += '  </td>  ';
        html += '  <td rowspan="4" >   ';

        html += '  <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" >';

        html += '  <li><input type="checkbox" name="bringshipping_weight[' + module_row + '][all_customers]"  value="1"  /> <?php echo $text_all_customers; ?> </li>  ';


        <?php if ($groups) { ?>
        <?php foreach ($groups as $group) { ?>

        html += '  <li ><input type="checkbox" name="bringshipping_weight[' + module_row + '][customer_groups][]"  value="<?php echo $group['customer_group_id']?>"  /> <?php echo $group['name']; ?></li>';
        <?php } ?>

        <?php }  ?>

        html += '       </ul>';
        html += '  </td>  ';
        ////////
        html += '  <td rowspan="4" >  ';

        html += '   <ul style="height: 100%; width: 250px; list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" > ';


        html += '<li><input type="checkbox" name="bringshipping_weight[' + module_row + '][all_zones]"  value="1" /> <?php echo $text_all_zones; ?> </li>  ';

        <?php foreach ($geo_zones as $geo_zone) { ?>


        html += '    <li> ';

        html += '<input type="checkbox" name="bringshipping_weight[' + module_row + '][geo_zones][]" value="<?php echo $geo_zone['geo_zone_id']?>"  /> <?php echo $geo_zone['name']; ?>';


        html += '    </li> ';
        <?php } ?>


        html += '   </ul>  ';
        html += '  </td>  ';

        //////
        ////////
        html += '  <td rowspan="4" >  ';

        html += '   <ul style="height: 100%; width: 250px; list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" > ';


        // html += '<li><input type="checkbox" name="bringshipping_weight['+ module_row +'][all_zones]"  value="1" /> <?php echo $text_all_zones; ?> </li>  ';

        <?php foreach ($geo_zones as $geo_zone) { ?>


        html += '    <li> ';

        html += '<input type="checkbox" name="bringshipping_weight[' + module_row + '][geo_zones_excluded][]" value="<?php echo $geo_zone['geo_zone_id']?>"  /> <?php echo $geo_zone['name']; ?>';


        html += '    </li> ';
        <?php } ?>


        html += '   </ul>  ';
        html += '  </td>  ';

        //////


        html += '  <td rowspan="4" >  ';

        html += ' <?php echo $entry_min_limit; ?>';
        html += ' <input type="text" name="bringshipping_weight[' + module_row + '][cart_min_limit]" size="10" /> <br/> <br/>';
        html += '<?php echo $entry_max_limit; ?>';


        html += '<input type="text" name="bringshipping_weight[' + module_row + '][cart_max_limit]" size="10" /><br/> <br/>';

        html += '  </td>  ';


        html += '  <td rowspan="4" >  ';

        html += '<textarea name="bringshipping_weight[' + module_row + '][rates]" cols="40" rows="5"></textarea>';

        html += '  </td>  ';

        html += ' <td rowspan="4">';

        html += '   <?php echo $use_dates; ?>  ';

        html += '   <input type="checkbox" name="bringshipping_weight[' + module_row + '][use_dates]" class="use_dates" />';
        html += '  <br/> ';

        html += ' <?php echo $date_start; ?><br/>';
        html += '<input type="text" name="bringshipping_weight[' + module_row + '][date_start]" class="datetime" style="display:none;" /> <br/>';


        html += '  <?php echo $date_end; ?><br/>';
        html += ' <input type="text" name="bringshipping_weight[' + module_row + '][date_end]" class="datetime" style="display:none;" /> ';

        html += '  </td> ';


        html += '  <td>  ';
        html += '  </td>  ';
        html += '  </tr>';
        html += '  <tr>  ';
        html += '  <td>  <?php echo $entry_title; ?>    ';
        html += '<input type="text" name="bringshipping_weight[' + module_row + '][title]"  size="25" />';

        html += '  </td>  ';
        html += '  <td class="right"> <a onclick="$(\'#module-row' + module_row + '\').remove();" class="button">Remove</a> ';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr>  ';
        html += '  <td> <?php echo $entry_tax_class; ?> ';
        html += '      <select name="bringshipping_weight[' + module_row + '][tax_class]">';


        html += '<option value="0"><?php echo $text_none; ?></option>';
        <?php foreach ($tax_classes as $tax_class) { ?>


        html += ' <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>';


        <?php } ?>


        html += ' </select> </td> ';


        html += '  <td rowspan="2" >  ';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr>  ';
        html += '  <td>  <?php echo $entry_sort_order; ?>    ';
        html += '<input type="text" name="bringshipping_weight[' + module_row + '][sort_order]"  size="3" />';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr style="height: 15px;" ><td colspan="9" style="background-color: #EFEFEF;"></td></tr>';

        html += '</tbody>';

        $('#module tfoot').before(html);

        $('#module-row' + module_row + ' .datetime').datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'h:m'
        });


        module_row++;
    }
</script>

<script type="text/javascript">
    var flat_row =<?php echo $flat_row; ?>;

    function addFlat() {

        html = '<tbody id="flat-row' + flat_row + '">';
        html += '  <tr>  ';
        html += '  <td> <?php echo $entry_status; ?>';
        html += '      <select name="bringshipping_flat[' + flat_row + '][status]">';
        html += '<option value="0"><?php echo $text_disabled; ?></option>   ';
        html += '<option value="1"><?php echo $text_enabled; ?></option>   ';
        html += ' </select> </td> ';
        html += '  <td rowspan="4" >  ';

        <?php if ($stores) { ?>
        html += ' <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" >';
        <?php foreach ($stores as $store) { ?>
        html += '   <li ><input type="checkbox" name="bringshipping_flat[' + flat_row + '][stores][]"  value="<?php echo $store['store_id']?>"  /> <?php echo $store['name']; ?></li>';
        <?php } ?>
        html += '  </ul>';
        <?php }  ?>

        html += '  </td>  ';
        html += '  <td rowspan="4" >   ';
        html += '  <ul style="list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" >';
        html += '  <li><input type="checkbox" name="bringshipping_flat[' + flat_row + '][all_customers]"  value="1"  /> <?php echo $text_all_customers; ?> </li>  ';

        <?php if ($groups) { ?>
        <?php foreach ($groups as $group) { ?>
        html += '  <li ><input type="checkbox" name="bringshipping_flat[' + flat_row + '][customer_groups][]"  value="<?php echo $group['customer_group_id']?>"  /> <?php echo $group['name']; ?></li>';
        <?php } ?>
        <?php }  ?>

        html += '       </ul>';
        html += '  </td>  ';
        html += '  <td rowspan="4" >  ';
        html += '   <ul style="height: 100%; width: 250px; list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" > ';
        html += '<li><input type="checkbox" name="bringshipping_flat[' + flat_row + '][all_zones]"  value="1" /> <?php echo $text_all_zones; ?> </li>  ';

        <?php foreach ($geo_zones as $geo_zone) { ?>
        html += '    <li> ';
        html += '<input type="checkbox" name="bringshipping_flat[' + flat_row + '][geo_zones][]" value="<?php echo $geo_zone['geo_zone_id']?>"  /> <?php echo $geo_zone['name']; ?>';
        html += '    </li> ';
        <?php } ?>

        html += '   </ul>  ';
        html += '  </td>  ';
        html += '  <td rowspan="4" >  ';
        html += '   <ul style="height: 100%; width: 250px; list-style-type: none; text-align:left!important; -webkit-padding-start: 0px; margin:0px;padding:0px;" > ';

        <?php foreach ($geo_zones as $geo_zone) { ?>
        html += '    <li> ';
        html += '<input type="checkbox" name="bringshipping_flat[' + flat_row + '][geo_zones_excluded][]" value="<?php echo $geo_zone['geo_zone_id']?>"  /> <?php echo $geo_zone['name']; ?>';
        html += '    </li> ';
        <?php } ?>

        html += '   </ul>  ';
        html += '  </td>  ';
        html += '  <td rowspan="4" >  ';
        html += ' <?php echo $entry_min_limit; ?>';
        html += ' <input type="text" name="bringshipping_flat[' + flat_row + '][cart_min_limit]" size="10" /> <br/> <br/>';
        html += '<?php echo $entry_max_limit; ?>';
        html += '<input type="text" name="bringshipping_flat[' + flat_row + '][cart_max_limit]" size="10" /><br/> <br/>';
        html += '  </td>  ';
        html += '  <td rowspan="4" >  ';
        html += '  <input type="text" name="bringshipping_flat[' + flat_row + '][flat_rate]"   />';
        html += ' <br/>  <br/>';
        html += '  <select name="bringshipping_flat[' + flat_row + '][type]">';
        html += '    <option value="per_item" ><?php echo $text_per_item; ?></option>';
        html += ' <option value="flat_rate" selected="selected" ><?php echo $text_flat_rate; ?></option>';
        html += '   </select>';
        html += '  </td>  ';
        html += ' <td rowspan="4">';
        html += ' <?php echo $use_dates; ?>  ';
        html += '   <input type="checkbox" name="bringshipping_flat[' + flat_row + '][use_dates]" class="use_dates" />';
        html += '  <br/> ';
        html += ' <?php echo $date_start; ?><br/>';
        html += '<input type="text" name="bringshipping_flat[' + flat_row + '][date_start]" class="datetime" style="display:none;" /> <br/>';
        html += '  <?php echo $date_end; ?><br/>';
        html += ' <input type="text" name="bringshipping_flat[' + flat_row + '][date_end]" class="datetime" style="display:none;" /> ';
        html += '  </td> ';
        html += '  <td>  ';
        html += '  </td>  ';
        html += '  </tr>';
        html += '  <tr>  ';
        html += '  <td>  <?php echo $entry_title; ?>    ';
        html += '<input type="text" name="bringshipping_flat[' + flat_row + '][title]"  size="25" />';
        html += '  </td>  ';
        html += '  <td class="right"> <a onclick="$(\'#flat-row' + flat_row + '\').remove();" class="button">Remove</a> ';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr>  ';
        html += '  <td> <?php echo $entry_tax_class; ?> ';
        html += '      <select name="bringshipping_flat[' + flat_row + '][tax_class]">';
        html += '<option value="0"><?php echo $text_none; ?></option>';

        <?php foreach ($tax_classes as $tax_class) { ?>
        html += ' <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>';
        <?php } ?>

        html += ' </select> </td> ';
        html += '  <td rowspan="2" >  ';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr>  ';
        html += '  <td>  <?php echo $entry_sort_order; ?>    ';
        html += '<input type="text" name="bringshipping_flat[' + flat_row + '][sort_order]"  size="3" />';
        html += '  </td>  ';
        html += '  </tr>  ';
        html += '  <tr style="height: 15px;" ><td colspan="9" style="background-color: #EFEFEF;"></td></tr>';
        html += '</tbody>';

        $('#flat tfoot').before(html);
        $('#flat-row' + flat_row + ' .datetime').datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'h:m'
        });
        flat_row++;
    }
</script>

<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript">
    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
    });
</script>
<script type="text/javascript">
    $('#tabs a').tabs();
</script>
<script type="text/javascript"><!--

    $(document).ready(function () {

        $('.use_dates').each(function () {

            if ($(this).attr('checked') !== 'checked') {
                $(this).parent().find('.datetime').each(function () {

                    $(this).css('display', 'none');


                });

            }

        });
        //$('.datetime').css('display','none');


        $('.use_dates').live('click', function () {

            if ($(this).attr('checked') == 'checked') {
                $(this).parent().find('.datetime').each(function () {
                    $(this).css('display', 'block');
                });
            } else {

                $(this).parent().find('.datetime').each(function () {
                    $(this).css('display', 'none');
                });
            }
        });


    });
    //--></script>

<script type="text/javascript"><!--
    function submitForm() {
        var err = 0;
        $('.use_dates').each(function () {

            if ($(this).attr('checked') == 'checked') {
                $(this).parent().find('.datetime').each(function () {

                    if (!$(this).val()) {


                        $(this).css('background-color', 'red');
                        err++;
                    }


                });

            }

        });

        if (!err) {

            $('#form').submit();
        }


    }
    //--></script>
<?php echo $footer; ?> 