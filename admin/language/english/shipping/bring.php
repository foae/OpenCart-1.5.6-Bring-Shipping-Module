<?php

// Heading
$_['heading_title']    = '<span style="font-weight: bold; color: #009900;">[Hype]</span> Bring Shipping';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Bring shippong!';

// Content
$_['entry_price']                       = 'Price:';
$_['entry_tax_class']                   = 'Tax Class:';
$_['entry_geo_zone']                    = 'Geo Zone:';
$_['entry_status']                      = 'Send through Bring?';
$_['entry_site_url']                    = 'Site URL:';
$_['entry_site_url_helper']             = 'Auto generated';
$_['entry_from_postal_code']            = "Shop's Postal Code:";
$_['entry_from_postal_code_helper']     = 'Enter the postal code from where the shipment will be made';
$_['entry_sort_order']                  = 'Sort Order:';
$_['entry_min_price']                   = 'Min. cart value:';
$_['entry_min_price_helper']            = 'Minimum cart value for this option to become active. 0 for no limit.';
$_['entry_max_price']                   = 'Max. cart value:';
$_['entry_max_price_helper']            = 'Maximum cart value after which this shipping option is no longer available. Leave 0 for no limit.';
$_['tab_general']                       = 'General configuration';

// Error
$_['error_permission'] = 'You do not have enough permissions to modify Bring Shipping!';


// Transparent logo: http://www.postennorge.com/annual-report-2014/organisation/brands/_image/473888.png