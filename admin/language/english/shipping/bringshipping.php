<?php
// Heading
$_['heading_title'] = "Advanced Bring Shipping";

// Text
$_['text_shipping'] = 'Shipping';
$_['text_success'] = 'Success: You have modified weight based shipping by land!';
$_['text_default'] = '(Default)';
// Entry
$_['entry_rate'] = 'Rates:<br /><span class="help">Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..</span>';
$_['entry_tax_class'] = 'Tax Class:';
$_['entry_geo_zone'] = 'Geo Zone:';
$_['entry_exclude'] = 'Exclude Geo Zone:';

$_['entry_limit_cart'] = 'Cart limit :';

$_['entry_min_limit'] = 'Total:<br /><span class="help">The  MINIM  checkout total the order must reach BEFORE this payment method becomes ACTIVE.</span>';
$_['entry_max_limit'] = 'Total:<br /><span class="help">The MAXIM checkout total the order must reach AFTER this payment method becomes INACTIVE.</span>';


$_['entry_status'] = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_groups'] = 'Groups:';
$_['entry_store'] = 'Store:';
$_['entry_general'] = 'General:';
$_['entry_title'] = 'Title:';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify weight based shipping!';
$_['use_dates'] = 'Use dates ?';

$_['text_all_customers'] = 'All customers';
$_['text_all_zones'] = 'All zones';


$_['date_period'] = 'Entry date:';

$_['date_start'] = 'Date start:';
$_['date_end'] = 'Date end:';

$_['tab_general'] = 'General:';
$_['tab_weight'] = 'Weight  modules:';
$_['tab_flat'] = 'Flat rate modules:';
$_['entry_flat'] = 'Entry cost:';
$_['text_flat_rate'] = 'Flat rate:';
$_['text_per_item'] = 'Per item:';
