<?php

class ModelShippingBringShipping extends Model
{

    public $baseUrl;

    public function getQuote($address)
    {

        $this->load->language('shipping/bringshipping');
        $this->load->model('setting/extension');

        // Totals
        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        // Setup the shop's URL
        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $this->baseUrl = $this->config->get('config_ssl');
        } else {
            $this->baseUrl = $this->config->get('config_url');
        }

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
            }
        }

        $current_store = $this->config->get('config_store_id');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
        foreach ($query->rows as $result) {
            $all_zones[] = $result['geo_zone_id'];
        };

        $shipping = array();
        if ($this->config->get('bringshipping_weight')) {
            $shipping['bringshipping_weight'] = $this->config->get('bringshipping_weight');

        }
        if ($this->config->get('bringshipping_flat')) {
            $shipping['bringshipping_flat'] = $this->config->get('bringshipping_flat');

        }

        $quote_data = array();

        foreach ($shipping as $index => $value) {

            if ($this->config->get($index . '_status')) {
                foreach ($value as $key => $module) {

                    $stores = array();
                    $check_group = false;
                    $geo_zones = array();
                    $use_dates = false;
                    $geo_zones_excluded = array();
                    $limit_cart_total = true;

                    /* compare total cart */
                    if ($module['cart_min_limit'] != '' && $module['cart_min_limit'] >= $total) {
                        $limit_cart_total = false;
                    } elseif ($module['cart_max_limit'] != '' && $module['cart_max_limit'] <= $total) {
                        $limit_cart_total = false;
                    }


                    /* store */
                    if (isset($module['stores'])) {
                        $stores = $module['stores'];
                    }
                    /* customer groups */
                    if (isset($module['customer_groups'])) {
                        $check_group = in_array($this->customer->getCustomerGroupId(), $module['customer_groups']);
                        if ($this->customer->getCustomerGroupId() == '' && in_array('1', $module['customer_groups'])) {
                            $check_group = true;
                        }
                    }
                    if (isset($module['all_customers']) && $module['all_customers']) {
                        $check_group = true;

                    }


                    /* geo zones */
                    if (isset($module['geo_zones'])) {

                        $geo_zones = $module['geo_zones'];

                    }
                    if (isset($module['all_zones']) && $module['all_zones']) {
                        $geo_zones = $all_zones;

                    }
                    /* geo zones  excluded */

                    if (isset($module['geo_zones_excluded'])) {

                        foreach ($module['geo_zones_excluded'] as $zone_id) {
                            $query = $this->db->query("SELECT zone_id,country_id FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$zone_id . "' ");
                            if ($query->num_rows) {
                                foreach ($query->rows as $row) {

                                    if ($row['zone_id'] == 0) {
                                        $all_cityes = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "zone WHERE country_id = '" . $row['country_id'] . "' ");

                                        foreach ($all_cityes->rows as $city) {

                                            $geo_zones_excluded[] = $city['zone_id'];
                                        }


                                    } else {
                                        $geo_zones_excluded[] = $row['zone_id'];
                                    }
                                }
                            }

                        }
                    }


                    /* date available  */

                    if (isset($module['use_dates']) && $module['use_dates']) {


                        if (!$module['date_start']) {
                            $module['date_start'] = '0000-00-00 00:00:00';
                        }
                        if (!$module['date_end']) {
                            $module['date_end'] = '0000-00-00 00:00:00';
                        }

                        $query = $this->db->query("SELECT '" . $module['date_start'] . "' < NOW() AND NOW() <  '" . $module['date_end'] . "' as answer");

                        $x = $query->row;

                        $use_dates = $x['answer'];

                    } else {
                        $use_dates = true;
                    }


                    if ($module['status'] && in_array($current_store, $stores) && $check_group && !empty($geo_zones) && $use_dates && $limit_cart_total) {


                        foreach ($geo_zones as $zone_id) {

                            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$zone_id . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

                            if ($query->num_rows) {
                                $status = true;


                                if (in_array($address['zone_id'], $geo_zones_excluded)) {
                                    $status = false;

                                }

                            } else {
                                $status = false;
                            }


                            if ($status) {
                                $cost = '';
                                $weight = $this->cart->getWeight();
                                // rates
                                if (isset($module['rates'])) {


                                    $rates = explode(',', $module['rates']);

                                    foreach ($rates as $rate) {
                                        $data = explode(':', $rate);

                                        if ($data[0] >= $weight) {
                                            if (isset($data[1])) {
                                                $cost = $data[1];
                                            }

                                            break;
                                        }
                                    }

                                } elseif (isset($module['flat_rate'])) {

                                    if (isset($module['type']) && $module['type'] == 'per_item') {

                                        $cost = $module['flat_rate'] * $this->cart->countProducts();

                                    } elseif (isset($module['type']) && $module['type'] == 'flat_rate') {

                                        $cost = $module['flat_rate'];

                                    }

                                }

                                if ((string)$cost != '') {
                                    if ($index == 'bringshipping_weight') {
                                        $greutate = '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')';
                                    } else {
                                        $greutate = '';
                                    }

                                    /*
                                     * Generate pickup points
                                     */
                                    $countryCode = $address['iso_code_2'];

                                    // Retain the country code in session - will be needed to fetch data from bring on checkout/success page
                                    $this->session->data['bringCountryCode'] = $countryCode;

                                    // Get the postal code
                                    $postalCode = (isset($address['postcode'])) ? (int) $address['postcode'] : 0;

                                    // Validates the postal code & generates 10 pickup points
                                    $pickupPoints = $this->generatePickupPoints($countryCode, $postalCode);

                                    if ($pickupPoints && is_array($pickupPoints)) {

                                        foreach ($pickupPoints as $pickupPoint => $details) {
                                            // Add the pickup point ID to the shipping description.
                                            // We are later querying Bring (checkout/success) by this pickup point ID
                                            $quote_data['bring_pickup_point_' . $details['id']] = array(
                                                'code' => 'bringshipping.bring_pickup_point_' . $details['id'],
                                                'title' => "{$details['name']}, City: {$details['city']}, Address: {$details['address']}, Postal Code: {$details['postalCode']}, Open [{$details['openingHoursNorwegian']}], <a href='{$details['googleMapsLink']}' target='_blank'>View on Google Maps</a> ",
                                                'cost' => $cost,
                                                'tax_class_id' => $module['tax_class'],
                                                'text' => $this->currency->format($this->tax->calculate($cost, $module['tax_class'], $this->config->get('config_tax')), $this->session->data['currency'])
                                            );
                                        }
                                    } else {

                                        // Bring pickup points are not available,
                                        // display an ordinary shipping options
                                        $quote_data['bringshipping_' . $index . "_" . $key] = array(
                                            'code' => 'bringshipping.bringshipping_' . $index . "_" . $key,
                                            'title' => $module['title_regular'] . $greutate,
                                            'cost' => $cost,
                                            'tax_class_id' => $module['tax_class'],
                                            'text' => $this->currency->format($this->tax->calculate($cost, $module['tax_class'], $this->config->get('config_tax')), $this->session->data['currency'])
                                        );
                                    }


                                }

                            }
                        }


                    }
                }
            }
        }


        $method_data = array();

        if ($quote_data) {
            $method_data = array(
                'code' => 'bringshipping',
                'title' => $this->language->get('text_title'),
                'quote' => $quote_data,
                'sort_order' => $module['sort_order'],
                'error' => false
            );
        }


        return $method_data;
    }



    /**
     * Requests 10 pickup points from Bring API
     * Checks whether the country is supported & the postal code is correct
     * @param $countryCode
     * @param $postcode
     * @return string error message
     */
    public function generatePickupPoints($countryCode, $postcode) {

        $postcode = filter_var($postcode, FILTER_SANITIZE_NUMBER_INT);
        $countryCode = filter_var($countryCode, FILTER_SANITIZE_STRING);
        $availableCountries = array('NO', 'DK', 'SE', 'FI');

        if (in_array($countryCode, $availableCountries) && $this->validatePostalCode($countryCode, $postcode)) {

            // We have a valid postal code and the country is supported - we can display 10 pickup points
            // Get cURL resource
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.bring.com/pickuppoint/api/pickuppoint/{$countryCode}/postalCode/{$postcode}.json",
                CURLOPT_USERAGENT => 'cURL'
            ));
            // Send the request & save response
            $response = curl_exec($curl);
            curl_close($curl);
            // Return an array this time
            $formattedResponse = json_decode($response, true);
            // un-nest a little
            return $formattedResponse['pickupPoint']; // 10 arrays (pickup points)

        } else {
            // No pickup points have been returned
            return false;
        }

    }



    /**
     * Checks if the country is supported by Bring +
     * Checks if the postal code is correct
     * @param $countryCode
     * @param $postcode
     * @return string city | bool false
     */
    public function validatePostalCode($countryCode, $postcode) {

        $postcode = filter_var($postcode, FILTER_SANITIZE_NUMBER_INT);
        $countryCode = filter_var($countryCode, FILTER_SANITIZE_STRING);
        $availableCountries = array('NO', 'DK', 'SE', 'FI', 'NL', 'DE', 'US', 'BE', 'FO', 'GL');

        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.bring.com/shippingguide/api/postalCode.json?clientUrl={$this->baseUrl}&country={$countryCode}&pnr={$postcode}",
            CURLOPT_USERAGENT => 'cURL'
        ));
        // Send the request & save response
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);

        // For unsupported countries (countries not in the list above),
        // all postal codes will be marked as valid with no city name returned,
        // therefore we return false (as it would be an invalid postal code)
        if (in_array($countryCode, $availableCountries) && $response->valid) {
            // Return the city for this postal code
            return $response->result;
        } else {
            return false;
        }

    }


}
